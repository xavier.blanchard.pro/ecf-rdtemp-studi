<?php

namespace App\Controller\Admin;

use App\Entity\DataHygro;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DataHygroCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DataHygro::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
