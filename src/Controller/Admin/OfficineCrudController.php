<?php

namespace App\Controller\Admin;

use App\Entity\Officine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class OfficineCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Officine::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('RaisonSociale'),
            TextField::new('Ville'),
            TextField::new('UserID'),
            AssociationField::new('chambreFroides'),
        ];
    }
}
