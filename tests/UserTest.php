<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user ->setUserName('Technicien-001')
            ->setPassword('pAssw0rd');

        $this->assertTrue($user->getUserName() ==='Technicien-001');
        $this->assertTrue($user->getPassword() === 'pAssw0rd');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user ->setUserName('Technicien-001')
            ->setPassword('pAssw0rd');

        $this->assertFalse($user->getUserName() ==='Officine-001');
        $this->assertFalse($user->getPassword() === 'MonMotDePasse');
    }
    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getUserName());
    }
}
