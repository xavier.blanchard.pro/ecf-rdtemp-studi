<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Officine;
use App\Entity\ChambreFroide;
use App\Entity\DataTemp;
use App\Entity\DataHygro;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        $url = $routeBuilder->setController(UserCrudController::class)->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('ECF STUDI RDTemp');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-list', User::class);
        yield MenuItem::linkToCrud('Officines', 'fas fa-list', Officine::class);
        yield MenuItem::linkToCrud('Chambres Froides', 'fas fa-list', ChambreFroide::class);
        yield MenuItem::linkToCrud('Datas Température', 'fas fa-list', DataTemp::class);
        yield MenuItem::linkToCrud('Datas Hygrométrie', 'fas fa-list', DataHygro::class);
    }
}
