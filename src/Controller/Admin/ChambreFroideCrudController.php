<?php

namespace App\Controller\Admin;

use App\Entity\ChambreFroide;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ChambreFroideCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ChambreFroide::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
