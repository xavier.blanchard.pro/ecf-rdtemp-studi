<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Officine;
use App\Entity\ChambreFroide;
use App\Entity\DataTemp;
use App\Entity\DataHygro;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use Faker\Factory;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // utilisation de Faker
        $faker = factory::create('fr_FR');

        // création d'un utilisateur Admin

        $user = new User();

        $user ->setUserName('XavierBlanchard');

        $password = $this->encoder->encodePassword($user, 'password');
        $user ->setPassword($password);

        $manager->persist($user);

        // création des utilisateurs Officines

        for ($i=0; $i < 10; $i++) {
            $officine = new Officine();
            $userOfficine = new User();

            $userOfficine ->setUserName($faker->numerify('OFFICINE-####'));

            $passwordOfficine = $this->encoder->encodePassword($userOfficine, 'password');
            $userOfficine ->setPassword($passwordOfficine);

            $officine  ->setRaisonSociale($faker->company())
            ->setAdresse($faker->streetAddress())
            ->setCodePostal($faker->postcode())
            ->setVille($faker->city())
            ->setTelephone($faker->phonenumber())
            ->setUserID($userOfficine);

            // création des chambres froides

            for ($j=0; $j < 5; $j++) {
                $chambreFroide = new ChambreFroide();

                $chambreFroide  ->setNom($faker->numerify('ChambreFroide-###'))
                    ->setTempMin('1.00')
                    ->setTempMax('9.00')
                    ->setHygroMin('10.00')
                    ->setHygroMax('90.00')
                    ->setOfficineID($officine);

                // création des dataTemp et dataHygro

                for ($k=0; $k < 100; $k++) {
                    $dataTemp = new DataTemp();
                    $dataHygro = new DataHygro();
                    $date1 = $faker->dateTime();

                    $dataTemp  ->setDateHeure($date1)
                        ->setValeur($faker->randomFloat(2, 0, 10))
                        ->setValidation(false)
                        ->setChambreFroideID($chambreFroide);
                    
                    $dataHygro  ->setDateHeure($date1)
                        ->setValeur($faker->randomFloat(2, 0, 100))
                        ->setValidation(false)
                        ->setChambreFroideID($chambreFroide);
                
                    $manager->persist($dataTemp);
                    $manager->persist($dataHygro);
                }

                $manager->persist($chambreFroide);
            }
            
            $manager->persist($userOfficine);
            $manager->persist($officine);
        }

        // création des utilisateurs Techniciens

        for ($i=0; $i < 5; $i++) {
            $userTech = new User();

            $userTech ->setUserName($faker->numerify('TECHNICIEN-####'));
                
            $passwordTech = $this->encoder->encodePassword($userTech, 'password');
            $userTech ->setPassword($passwordTech);
    
            $manager->persist($userTech);
        }
        
        // lancer la création en base de données

        $manager->flush();
    }
}
