<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OfficineController extends AbstractController
{
    #[Route('/officine', name: 'officine')]
    public function index(): Response
    {
        return $this->render('officine/index.html.twig', [
            'controller_name' => 'OfficineController',
        ]);
    }
}
